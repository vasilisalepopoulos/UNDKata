import sys
import time
import urllib.request
import openpyxl
import pandas as panda
from openpyxl import load_workbook


def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('\r[%s] %s%s ...%s\r' % (bar, percents, '%', status))

    sys.stdout.flush()



def create_excel_file(directory):
        stock_file = panda.read_excel(directory+"/und_wh.xlsx")  # open the excel File
        stock_file.columns = stock_file.iloc[0]
        stock_file = stock_file[1:]  # Deletes the first header of the files
        backorder_file = panda.read_excel(directory+"/und_bo.xlsx")  # open the excel File
        database_file = panda.read_excel(directory+"/Database Funko.xlsx")
        last_file = panda.merge(stock_file,backorder_file, on ='Code')
        last_file = panda.merge(last_file,database_file, on='Code')
        last_file = last_file[['Code', 'images', 'Περιγραφή Είδους', 'Barcode', 'Τιμή Πωλ. 1', 'Υπόλοιπο', 'this']]
        last_file.insert(2,"","")
        last_file.columns = ['Κωδικός', 'url', 'Προϊόν',  'Περιγραφή', 'Barcode', 'ΠΤΛ', 'Υπόλοιπο', 'Αναμενόμενη']
        last_file = last_file[~last_file['Περιγραφή'].str.contains('\*')]
        last_file = last_file[~last_file['Περιγραφή'].str.contains('Λούτρινο')]
        last_file = last_file[~last_file['Περιγραφή'].str.contains('Λούτρινα')]
        last_file = last_file[~last_file['Περιγραφή'].str.contains('Κούπα')]
        last_file = last_file.drop(last_file[(last_file['Υπόλοιπο'] < 10) & (last_file['Αναμενόμενη'] < 6)].index)
        last_file.to_excel(directory+"\\out.xlsx", index=False)


def insert_images(directory):
    file_name = '\\out.xlsx'
    workbook = load_workbook(directory+file_name)
    worksheet = workbook["Sheet1"]
    row_count = worksheet.max_row
    worksheet.column_dimensions['C'].weidth = 100

    for i in range(2, row_count):
        progress(i,row_count, status = "Downloading images")
        time.sleep(0.5)
        worksheet.row_dimensions[i].height = 100

        image_name = worksheet.cell(row= i, column = 1).value
        image_link = worksheet.cell(row= i, column = 2).value
        if(image_link[-4:]=='.jpg'):
            image_type = '.jpg'
        else:
            image_type = '.png'

        image_loc = directory+"\\"+image_name+image_type


        try:
            url = urllib.parse.urlparse(image_link)
            url = url.scheme + "://" + url.netloc + urllib.parse.quote(url.path)
            urllib.request.urlretrieve(url, image_loc)
            image = openpyxl.drawing.image.Image(image_loc)
            image.height = 65
            image.width = 65
            worksheet.add_image(image, worksheet.cell(row=i, column=3).coordinate)

        except urllib.error.HTTPError:
            worksheet.cell(row=i, column=3).value = '404'




    print("Saving POP price list")
    workbook.save(directory+'\POP pricelist.xlsx')
    print("DONE!")


