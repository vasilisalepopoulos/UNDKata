import glob
import time
import urllib
import logging
from Services import ImageService
import Constants


class Image:
    logging.getLogger(__name__)
    name = None
    barcode = None
    large_file = False
    small_file = False
    small_images_directory = None
    large_images_directory = None
    image_service = None
    small_file_name = None
    large_file_name = None

    def __init__(self, directory):
        self.small_images_directory = directory + Constants.SMALL_IMAGES_DIR
        self.large_images_directory = directory + Constants.LARGE_IMAGES_DIR
        self.image_service = ImageService.ImageService(directory)

    def set_variables(self, name, barcode):
        self.name = name
        self.barcode = barcode if self.is_barcode(barcode) else print("")

    def check_for_small_file(self):
        if glob.glob(self.small_images_directory + self.name + ".*"):
            self.small_file = True
            self.small_file_name = glob.glob(self.small_images_directory + self.name + ".*")[0]
            return True
        else:
            return False

    def check_for_large_file(self):
        if glob.glob(self.large_images_directory + self.name + ".*"):
            self.large_file = True
            self.large_file_name = glob.glob(self.small_images_directory + self.name + ".*")[0]
            return True
        else:
            return False

    def get_small_file_name(self):
        return self.small_file_name

    def get_large_file_name(self):
        return self.large_file_name

    def download_image(self, image_link):
        try:
            file = self.define_type(image_link)
            image_link = urllib.parse.urlparse(image_link)
            image_link = image_link.scheme + "://" + image_link.netloc + urllib.parse.quote(image_link.path)
            urllib.request.urlretrieve(image_link, self.large_images_directory + "%s" % file)
            time.sleep(3)
            self.image_service.compress_image(file)
        except (OSError, TypeError) as e:
            logging.error('"Code": "158",'
                          '"Product"":"{}",'
                          '"URL":"{}",'
                          '"Error":"{}"'.format(self.name, image_link, e))

    # Deletes the initial 0 that is found on some of the EAN codes
    def fix_barcode(self):
        try:
            if self.barcode[:1] == "0" and len(self.barcode) == 13:
                return self.barcode[1:]
            else:
                return self.barcode
        except:
            logging.error('"Code": "169",'
                          '"Product":"{}",'
                          '"Barcode":"{}"'.format(self.name, self.barcode))

    def is_barcode(self, barcode):  # Checks that the format of the barcode is valid (12-13 characters all numbers)
        results = []
        final = True

        if barcode is not None:

            if 13 >= len(barcode) >= 12:
                results.append(True)
            else:
                results.append(False)
            try:
                int(barcode)
                results.append(True)
            except ValueError as e:
                results.append(False)

            for v in results:
                final = final and v

        else:
            final = False
        return final

    def define_type(self, url):
        if url is not None:
            if url[-4:] == '.jpg':
                image_type = '.jpg'
                file = self.name + image_type
            elif url[-4:] == '.png':
                image_type = ".png"
                file = self.name + image_type
            else:
                image_type = ".jpg"
                file = self.name + image_type

            return file

