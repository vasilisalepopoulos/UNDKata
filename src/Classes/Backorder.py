import glob
import os
import Constants
import pandas as pd


class Backorder:
    file_directory = None
    backorder_df = pd.DataFrame()
    final_df = pd.DataFrame()
    sku_column = 'Unnamed: 3'
    stock_column = 'Unnamed: 5'

    def __init__(self, directory):

        self.file_directory = directory + Constants.BACKORDERS_DIR
        os.chdir(self.file_directory)
        extension = 'xlsx'
        filenames = [i for i in glob.glob('*.{}'.format(extension))]

        for f in filenames:
            self.backorder_df = pd.read_excel(f)
            self.backorder_df = self.backorder_df.iloc[5:]
            self.final_df = pd.concat([self.final_df, self.backorder_df], sort=True)

        self.final_df = self.final_df[[self.sku_column, self.stock_column]]
        self.final_df = self.final_df.iloc[2:]
        self.final_df = self.final_df[self.final_df[self.sku_column].notna()]
        self.final_df.columns = ['Product Code', 'Backorder']

    def get_backorder(self):

        return self.final_df



