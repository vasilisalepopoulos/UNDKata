import logging
import pandas as pd
import Constants


class Stock:
    file_directory = None
    stock_df = pd.DataFrame()
    final_df = pd.DataFrame()
    logging.getLogger(__name__)

    def __init__(self, directory):
        self.file_directory = directory + Constants.STOCK_DIR
        self.stock_df = pd.read_excel(self.file_directory+'\\wh.xlsx')
        self.stock_df = self.stock_df[['EID_CODE', 'EID_PERIGR', 'BARCODE', 'POSOT', 'TIM_POL1']]
        self.stock_df.columns = ['Product Code', 'Description', 'Barcode', 'Stock', 'SRP']
        self.stock_df = self.stock_df[self.stock_df['Product Code'].notna()]
        self.remove_retail_codes_by_name()
        self.fix_minus_stock()

    def get_stock(self):
        return self.final_df

    def remove_retail_codes_by_name(self):

        for code in Constants.NOT_NEEDED:
            self.final_df = pd.concat([self.final_df,
                                       self.stock_df[self.stock_df['Product Code'].str.startswith(code) &
                                                    ~self.stock_df['Description'].str.contains(Constants.NOT_NEEDED[code])]],
                                      ignore_index=True)
        logging.info("removed all products that are not necessary")

    def fix_minus_stock(self):
        self.final_df.loc[self.final_df['Stock'] < 0, 'Stock'] = 0
        logging.info("fixed quantities with values less than 0 (zero)")



