import os

from setuptools import setup, find_packages

import Constants

try:
    setup(
        name="UNDKata",
        version="0.1",
        packages=find_packages(),
        scripts=["FileService.py", "ImageService.py"],

        # Project uses reStructuredText, so ensure that the docutils get
        # installed or upgraded on the target machine
        install_requires=["docutils>=0.3", "Openpyxl", "pandas","selenium","xlrd>-1.0.0", "pillow"],
        package_data={
            # If any package contains *.txt or *.rst files, include them:
            "": ["*.txt", "*.rst"],
            # And include any *.msg files found in the "hello" package, too:
            "hello": ["*.msg"],
        },

        # metadata to display on PyPI
        author="Vasilis Alepopoulos",
        author_email="vasilisalepopoulos@gmail.com",
        description="This software generates a pricelist for all the products"
                    " that the organization sells with the help of the files "
                    "generated from its ERP and e-commerce site and some REST APis",
        keywords="",
        url="",   # project home page, if any
        project_urls={},
        classifiers=[
            "It'a a waste of time, that's why I need to save some :) "
        ]

        # could also include long_description, download_url, etc.
    )
finally:
    path = input("Path: ")
    os.mkdir(path + "\\Gollem")
    for directory in Constants.DIRECTORIES:
        full_path = path + "\\Gollem" +directory
        os.mkdir(full_path)