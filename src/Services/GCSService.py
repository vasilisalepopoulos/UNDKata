import json
import urllib
from urllib import request
import time
from difflib import SequenceMatcher
from urllib.request import Request
import logging


class gce:
    logging.getLogger(__name__)
    url = None
    value = None
    def __init__(self, item_name):
        api_key = 'AIzaSyBOlFRMlIZx3nl5Fl4cwRzdSh86xWYkabc'
        cx = '007950828091558018113:cerh9zixsul'
        self.value = str(item_name)
        self.url = 'https://www.googleapis.com/customsearch/v1?key=' + api_key + \
              "&cx=" + cx + \
              "&searchType=image" \
              "&q=" + self.value

    def return_link(self):
        return self.url

    def is_square(self, height, width):  # The aspect ratio must be 1:1
        return True if height - width<200 else False

    def has_correct_resolution(self, height, width):  # Resolution should be greater than 500x500
        return True if height >= 500 and width >= 500 else False

    def name_matches(self, dataframe_text, json_text):
        return True if SequenceMatcher(None, dataframe_text, json_text).ratio() >= 0.8 else False

    def get_url_of_image(self):
        try:
            req = Request(self.url, headers={'User-Agent': 'Mozilla/5.0'})
            response = json.load(urllib.request.urlopen(req))
            if int(response.get("searchInformation").get('totalResults')) != 0:
                for r in response.get('items'):
                    if self.is_square(r.get('image').get('height'),
                                      r.get('image').get('width')) &\
                        self.has_correct_resolution(r.get('image').get('height'),
                                                    r.get('image').get('width')):

                        url_link = r.get('link')
                        return url_link
                        break

                time.sleep(3)
            else:
                return None
        except urllib.request.HTTPError:
            logging.info("An HTTPError occured while getting the image url of {}".format(self.value))

