import glob
import PIL
from PIL import Image
import os
from os import listdir
from os.path import isfile, join
import pandas as pd
import Constants
import logging


class ImageService:
    logging.getLogger(__name__)
    csv_directory = None
    output_directory = None
    image_directory = None
    small_images_directory = None
    large_images_directory = None
    image_instance = None

    def __init__(self, directory):
        self.csv_directory = directory + Constants.CSV_DIR
        self.output_directory = directory + Constants.OUTPUT_DIR
        self.image_directory = directory + Constants.IMAGES_DIR
        self.small_images_directory = directory + Constants.SMALL_IMAGES_DIR
        self.large_images_directory = directory + Constants.LARGE_IMAGES_DIR

    def merge_csv(self):
        extension = 'csv'
        os.chdir(self.csv_directory)
        all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
        combined_csv = pd.concat([pd.read_csv(f, sep='\t', lineterminator='\r') for f in all_filenames], sort=True)
        combined_csv.columns = ['Αδεια', 'URL', 'Περιγραφή', 'τιμη', 'Product Code']
        combined_csv.to_excel( self.output_directory + "\\image_db.xlsx", index=False)
        logging.info("All csv files from where merged")
        return combined_csv

    def compress_all_images(self):
        files = [f for f in listdir(self.large_images_directory) if isfile(join(self.large_images_directory, f))]
        for file in files:
            if glob.glob(self.small_images_directory+file+".*"):
                self.compress_image(file)
        logging.info("all images where compressed")

    def compress_image(self, file):
        try:

            image = Image.open(self.large_images_directory+file)
            width, height = image.size

            if width > 150 and height > 150:
                image = image.resize((150, 150), Image.ANTIALIAS)
                image = image.convert("RGB")
                image.save(self.small_images_directory+file, optimize=True, quality=95)

            logging.info("{} was compressed.".format(file))
        except PIL.UnidentifiedImageError:
            logging.info("An 'UNidentidifiedImageError occured' going to delete file")
            os.remove(self.large_images_directory+file)