import openpyxl
import pandas as panda
from openpyxl import load_workbook
from openpyxl.styles import Alignment, Border, Side
from openpyxl.styles.borders import BORDER_THIN
from Classes import Backorder, Image
from Classes.Stock import Stock
from Services.GCSService import gce
from Services import ImageService
import Constants
import logging


class FileService:
    logging.getLogger(__name__)
    pricelist = None
    directory = None
    stock_dataframe = None
    backorder_dataframe = None
    image = None
    image_service = None
    final_dataframe = panda.DataFrame
    destination_folder = None
    gce = None
    image_loc = None

    def __init__(self, directory):
        self.directory = directory
        c_stock = Stock(self.directory)
        c_backorder = Backorder.Backorder(self.directory)
        self.image_service = ImageService.ImageService(self.directory)
        self.destination_folder = self.directory + Constants.OUTPUT_DIR
        self.stock_dataframe = c_stock.get_stock()
        self.backorder_dataframe = c_backorder.get_backorder()
        self.urls_dataframe = self.image_service.merge_csv()
        self.image_service.compress_all_images()
        self.image = Image.Image(self.directory)

    def create_raw_pricelist(self):
        self.final_dataframe = self.merge_all()
        self.final_dataframe['Backorder'].fillna(0, inplace=True)
        self.save_to_file()
        logging.info("Created pricelist dataframe ")

    def merge_all(self):
        self.stock_dataframe = self.stock_dataframe.merge(self.backorder_dataframe, on='Product Code', how='left').merge(self.urls_dataframe, on='Product Code', how='left')
        self.stock_dataframe = self.stock_dataframe[['Product Code',
                                                     'URL',
                                                     'Barcode',
                                                     'Description',
                                                     'SRP',
                                                     'Stock',
                                                     'Backorder']]

        self.stock_dataframe = self.stock_dataframe.astype({'Barcode': 'object'})

        logging.info("merged all dataframes")
        return self.stock_dataframe

    def save_to_file(self):  # saves the final dataframe, which has all the necessary info, as an xlsx file.
        writer = panda.ExcelWriter(self.destination_folder+"\\raw_pricelist.xlsx",
                                   engine='xlsxwriter')

        for supplier in Constants.NOT_NEEDED:
            df = self.final_dataframe.loc[self.final_dataframe['Product Code'].str.startswith(supplier).values]
            df = df.drop_duplicates(subset="Product Code", keep='first')
            df = df.drop(df[(df['Stock'] < Constants.MINIMUM_STOCK) & (df['Backorder'] < Constants.MINIMUM_BACKORDER)].index)
            df.loc[df['Stock'] > 0, 'Stock'] = "Αμεσα Διαθέσιμο"
            df.loc[df['Stock'] == 0, 'Stock'] = "Αναμένεται"
            df.to_excel(writer, sheet_name=supplier, index=False)
        writer.save()
        writer.close()
        logging.info("saved the pricelist dataframe as an excel file")

    def insert_images(self):
        workbook = load_workbook(self.destination_folder+"\\raw_pricelist.xlsx")
        thin_border = Border(
            left=Side(border_style=BORDER_THIN, color='00000000'),
            right=Side(border_style=BORDER_THIN, color='00000000'),
            top=Side(border_style=BORDER_THIN, color='00000000'),
            bottom=Side(border_style=BORDER_THIN, color='00000000')
        )
        for supplier in Constants.WHOLESALE_PRODUCTS:
            worksheet = workbook[supplier]
            row_count = worksheet.max_row
            worksheet.insert_cols(3)  # inserts one more column to host the picture

            for row in worksheet:
                for cell in row:
                    cell.alignment = Alignment(horizontal='center', vertical='center')
                    cell.border = thin_border

            for index in range(2, row_count+1):
                self.image_loc = None
                worksheet.row_dimensions[index].height = 64

                product_name = worksheet.cell(row=index, column=1).value
                image_link = worksheet.cell(row=index, column=2).value
                product_barcode = worksheet.cell(row=index, column=4).value

                self.image.set_variables(product_name, product_barcode)
                product_barcode = self.image.fix_barcode()

                if self.image.check_for_small_file() is not True: #  This block of code checks if a picture should be downlaoded
                    if image_link is not None:  # Then it means that we already have a url from wixbox or toysforkids.
                        image_link = self.split_urls(image_link)
                    else: # It finds a link using a custom google search engine
                        self.image.set_variables(product_name, product_barcode)
                        gce_instance = gce(product_barcode)  # This line could cause a problem
                        image_link = gce_instance.get_url_of_image()

                    self.image.download_image(image_link)

                if self.image.check_for_small_file() is True:
                    self.image_loc = self.image.get_small_file_name()
                    excel_image = openpyxl.drawing.image.Image(self.image_loc)
                    excel_image.height = 80
                    excel_image.width = 80
                    worksheet.add_image(excel_image, worksheet.cell(row=index, column=2).coordinate)
                else:
                    continue

            worksheet.delete_cols(2)
            worksheet.delete_cols(7)
            worksheet.column_dimensions['A'].width = 12.14
            worksheet.column_dimensions['B'].width = 12
            worksheet.column_dimensions['C'].width = 13.43
            worksheet.column_dimensions['D'].width = 77.71
            worksheet.column_dimensions['E'].width = 8.43
            worksheet.column_dimensions['F'].width = 15.86


        print("Saving POP price list")
        logging.info("Inserted all images")
        workbook.save(self.directory + Constants.OUTPUT_DIR + '\\' + 'POP pricelist.xlsx')
        print("DONE!")

    # Returns the first url from the raw_pricelist file
    def split_urls(self, url):
        new_link = url.split("|", 6)[0]
        return new_link.strip()

    def export_log(self):
        file_path = self.directory + Constants.LOG + 'main_logger.txt'
        destination_path = self.directory+Constants.LOG+'log.xlsx'
        dataframe = panda.read_csv(file_path, sep=' - ', skiprows=1, engine='python')
        dataframe.columns = ['date', 'log_type', 'module', 'function', 'line', 'message']
        dataframe = dataframe[['message']].loc[dataframe['log_type'] == 'ERROR']  #  Keeps only the 'message' cell of each row that refers to an ERROR log message
        dataframe = panda.DataFrame(dataframe['message'].str.split(",").values.tolist())
        dataframe = dataframe.drop_duplicates(subset=1, keep='first')
        json = dataframe.to_json(orient='values')
        json = json.replace('\\', '')
        json = json.replace('""', '"')
        json = json.replace('[', '{')
        json = json.replace(']', '}')
        json = "[{}]".format(json[1:-1])
        json = json.replace('""', '"')
        json = json.replace("null", "")
        json = json.replace(',,,,,', '')
        print(json)
        j_dataframe = panda.read_json(json)
        writer = panda.ExcelWriter(destination_path,
                                   engine='xlsxwriter')
        j_dataframe.to_excel(writer, index=False)
        writer.save()
        writer.close()

# TODO focus on issue 21
