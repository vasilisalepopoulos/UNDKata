import Constants
from Services import FileService
import logging


def main():
    #  --------------  Logging Error Codes --------------------
    #  Error [158] --  Could not download image via given url
    #  Error [169] --  Product doesn't have barcode
    directory = input("Please give the folder path: ")
    logging.basicConfig(format='%(asctime)s - '
                               '%(levelname)s - '
                               '%(module)s - '
                               '%(funcName)s - '
                               '%(lineno)d - '
                               '%(message)s',
                        level=logging.INFO,
                        filename=directory+Constants.LOG +"main_logger.txt")
    logging.getLogger(__name__)
    logging.info('*************************************************************************************Started main')

    s_file = FileService.FileService(directory)
    s_file.create_raw_pricelist()
    s_file.insert_images()
    s_file.export_log()


if __name__ == "__main__":
    main()
