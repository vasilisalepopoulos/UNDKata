from unittest import TestCase

from Classes import Image


class TestImage(TestCase):
    directory = 'C:\\Users\\jedic\\Desktop\\UNDKata\\testing_files'
    c_image = Image.Image(directory)
    name = "UND555000"
    bc = "6956980544562"

    def test_set_variables(self):
        self.c_image.set_variables(self.name, self.bc)

        self.assertEqual(self.c_image.barcode, self.bc,
                         "Barcode should match")

        self.assertEqual(self.c_image.name, self.name,
                         "The names should be equal")

    def test_fix_barcode(self):
        new_bc = "0956980544562"
        self.c_image.set_variables(self.name, new_bc)
        self.c_image.fix_barcode()

        self.assertEqual("956980544562", self.c_image.fix_barcode(),
                         "Should be equals")

    def test_define_type(self):
        url = "asdfasdfasdf/asdfasdf/asdfa/image.jpg"
        self.c_image.set_variables(self.name, self.bc)
        self.assertEqual(self.c_image.define_type(url), "UND555000.jpg")

    def test_is_barcode_TRUE(self):
        valid_barcodes = ["321654987321",
                          "3216549873210",
                          "0321654987321",
                          "789456123456"]

        for b in valid_barcodes:
            self.assertTrue(self.c_image.is_barcode(b))

    def test_is_barcode_FALSE(self):
        valid_barcodes = ["3216",
                          "321654987321065465465465465465",
                          "0321654987321aaaaaa",
                          "789456123456²²³³",
                          "123412341§§§§§³²²³£³²£³§³£§£§€®€"]

        for b in valid_barcodes:
            self.assertFalse(self.c_image.is_barcode(b))