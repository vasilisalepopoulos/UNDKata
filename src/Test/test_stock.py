from unittest import TestCase

from pandas import testing
import pandas as pd

from Classes import Stock


class TestStock(TestCase):
    directory = "C:\\Users\\jedic\\Desktop\\UNDKata\\testing_files"

    def test_remove_retail_codes_by_name(self):
        stock_data = [['UND51512', 'POP Φιγούρα πεν', 6546546546540, 0, '32,22']]
        stock_df = pd.DataFrame(stock_data)
        stock_df.columns = ['Product Code', 'Description', 'Barcode', 'Stock', 'SRP']
        stock = Stock.Stock(directory=self.directory)

        print(stock.get_stock())
        print(stock_df)

        self.assertTrue(stock.get_stock().equals(stock_df))
