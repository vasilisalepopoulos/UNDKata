from unittest import TestCase

from Services.ImageService import ImageService
import pandas as pd


class TestImageService(TestCase):
    directory = 'C:\\Users\\jedic\\Desktop\\UNDKata\\testing_files'
    imageService = ImageService(directory)

    def test_merge_csv(self):
        pd.set_option('display.max_rows', 50)
        pd.set_option('display.max_columns', 5)
        pd.set_option('display.width', 1000)

        df = self.imageService.merge_csv()
        print("test")
        print(df)

    def test_compress_image(self):

        self.imageService.compress_all_images()


