from unittest import TestCase

import pandas

from Services.FileService import FileService


class TestFileService(TestCase):
    dir = 'C:\\Users\jedic\Desktop\\UNDKata\original_files'
    file_service = FileService(dir)

    def test_split_urls(self):
        url = "link 1 | link2 | link3"
        expected_url = self.file_service.split_urls(url)
        print(expected_url)
        assert expected_url == 'link 1'

    def test_log_export(self):
        pandas.set_option('display.max_rows', 500)
        pandas.set_option('display.max_columns', 500)
        pandas.set_option('display.width', 1000)
        self.file_service.export_log()